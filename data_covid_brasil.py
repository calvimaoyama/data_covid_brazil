#importacao de dados csv 
import pandas as pd
covid_df = pd.read_csv('owid-covid-data.csv')
print(covid_df)

#vizualizar o nome das colunas 
print(covid_df.columns.tolist())

#selecao de colunas 
covid_data_df = covid_df.loc[:,['location','date','total_cases']]
print(covid_data_df)

#dados de covid do Brazil
covid_brazil_df = covid_df['iso_code']=='BRA'
filtered_df = covid_df[covid_brazil_df]
print(filtered_df)


#dados colunas brasil   
covid_data_brasil_df = covid_df.set_index(['location','date','total_cases']).loc["Brazil"].sort_values('date')
print(covid_data_brasil_df)

